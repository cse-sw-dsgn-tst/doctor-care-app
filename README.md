# README #

# Doctor Care App #
To develop a working prototype for a Doctor Care Application for prescription management, 
featuring user stories to enable the doctors, patients and pharmacists 
to effectively handle prescriptions in a user-friendly system in 4 weeks.


## Getting Started ##
A quick introduction of the minimal setup you need to get your application up and running.

#### Programming Language(s) ####
* HTML
* CSS
* Java


#### Prerequisites ####
What is needed to set up the dev environment. For instance, global dependencies or any other tools. include download links.


#### Dev Environment ####
Here's a brief intro about what a developer must do in order to start developing the project further.


#### Code Style ####
Explain your code style and show how to check it..


#### Unit Tests ####
How to run unit tests


#### Deployment ####
How to run the app


## Definition of Done ##
List of criteria for a User Story to be considered done:

*  Code Peer-Review by >= 1 team member
*  Code Checked-In
*  Code Unit Tested with 80% code coverage
*  Code deployed to test environment
*  Acceptance Criteria met
*  User Documentation updated

## Branches ##

1. Branch out from develop
2. Perform work in a feature branch.
3. Never push into develop or master branch. Make a Pull Request.
4. Before making a Pull Request
	- Make sure your feature branch builds successfully and passes all tests (including code style checks).
	- Update your local develop branch and do an interactive rebase before pushing your feature.
	- Resolve potential conflicts while rebasing.
5. Delete local and remote feature branches after merging.

##### Develop #####
* The main development branch MUST be called `develop` in the main repo for the project.
* Changes MUST NOT be committed directly to the `develop` branch by anyone.
* Every contributor to the project MUST create a private fork of the repo.

##### Feature #####
* All work MUST be done on feature branches, one for each new feature or bug fix.
* All feature branches SHOULD be created on the developer private fork.
* Every feature branch MUST be branched from the up-to-date `develop` branch and it SHOULD be regularily rebased if there are any changes on `develop` before it gets merged.
* Feature and fix branches SHOULD be named using a convention:
	- fix/broken-link
	- feature/password-validation
* Feature branches MAY be named using convention:
	- broken-link
	- password-validation
* Feature branches MUST NOT be named in a way that requires additional knowledge to understand what they do.

## Commits ##

##### Commit Message #####
* A commit message MUST have an informative subject in its first line.
* A commit message SHOULD NOT need any context to understand what it does.

##### Commit Subject #####
* A commit subject MUST NOT exceed 72 characters.
* A commit subject MUST be in the form of: 
	- Do something -> for example: Add password length validation
* A commit subject MUST be capitalized and MUST NOT end with a period.

##### Commit Body #####
* A commit message MAY have a body with additional information.
* The body if present MUST be separated from the subject by one blank line.
* The body SHOULD NOT have any lines that exceed 72 characters (it MAY have longer lines only if it is impossible to format otherwise, e.g. with long URLs).
* The body MAY include links and references to issues, pull requests, other commits or more, if that is relevant only to this one commit. If that information relates to the entire pull request then that info SHOULD be present in the pull request description instead, and it should get commited as a commit message of the merge commit after the pull request gets merged.
* GitHub pull requests and issues in commit bodies or pull request descriptions MUST be referenced by #{NUMBER} and not a URL:
	- e.g.: #1234
* Other commits MUST be referenced by the commit hash (possibly abbreviated) and not a URL:
	- e.g.: a1b2c3d4

#### Examples ####
##### Good subject #####
- Add password length validation

##### Bad subject #####
- Add password length validation.
	- ends with a period
- Adding password length validation
	- wrong grammatical form
- add password length validation
	- is not capitalized
- Password length validation
	- wrong grammatical form
- Implement feature request from Friday
	- no information without additional context
- Task 1234
	- wrong grammatical form and no information without additional context

##### Good subject + body #####

> Add password length validation

> This commit adds validation required in Task-123
> using the abc validation library.

> It closes issue #456.

### Team ###
* Gerard Comerford
* Declan Kelly
* Jake Mulvany
* Babita Verma
* Ansar Yasmin

### Licensing ###
State what the license is and how to find the text version of the license.